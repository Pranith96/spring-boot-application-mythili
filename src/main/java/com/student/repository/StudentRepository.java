package com.student.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.student.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

	Optional<Student> findByStudentName(Integer studentId);

	@Modifying
	@Query("update Student s set s.studentName = :studentName where s.studentId = :studentId")
	void update(@Param("studentId") Integer studentId, @Param("studentName") String studentName);

}
