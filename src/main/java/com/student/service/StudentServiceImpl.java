package com.student.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.student.dto.StudentDto;
import com.student.entity.Student;
import com.student.exceptions.StudentNotFoundException;
import com.student.repository.StudentRepository;

@Service
@Transactional
@Profile(value = { "local", "prod", "dev" })
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepository repository;

	@Override
	public Student create(Student student) throws Exception {
		student.getAddress().setStudent(student);
		Student studentResponse = repository.save(student);
		if (studentResponse == null) {
			throw new Exception("student details not saved");
		}

		return studentResponse;
	}

	@Override
	public List<Student> getStudentData() throws Exception {
		List<Student> response = repository.findAll();
		if (response.isEmpty() || response == null) {
		}
		return response;
	}

	@Override
	public StudentDto getStudentDataByid(Integer studentId) throws StudentNotFoundException {
		Optional<Student> response = repository.findById(studentId);
		if (!response.isPresent()) {
			throw new StudentNotFoundException("student Id doesnot exists");
		}

		StudentDto studentDto = new StudentDto();
		studentDto.setStudentId(studentId);
		studentDto.setStudentName(response.get().getStudentName());
		studentDto.setStandard(response.get().getStandard());
		studentDto.setMobileNumber(response.get().getMobileNumber());
		studentDto.setStatus(response.get().getStatus());

		return studentDto;
	}

	@Override
	public Student update(Student student) throws Exception {
		Optional<Student> response = repository.findById(student.getStudentId());
		if (!response.isPresent()) {
			throw new StudentNotFoundException("student not found exists");
		}

		if ((student.getStudentName() != null)) {
			response.get().setStudentName(student.getStudentName());
		}
		if (student.getStandard() != null) {
			response.get().setStandard(student.getStandard());
		}
		if (student.getAddress() != null) {
			response.get().setAddress(student.getAddress());
		}
		if (student.getMobileNumber() != null) {
			response.get().setMobileNumber(student.getMobileNumber());
		}

		if (student.getStandard() != null) {
			response.get().setStandard(student.getStandard());
		}
		if (student.getStatus() != null) {
			response.get().setStatus(student.getStatus());
		}
		if (student.getPassword() != null) {
			response.get().setPassword(student.getPassword());
		}
		Student updatedresponse = repository.save(response.get());
		return updatedresponse;
	}

	@Override
	public String updateStudentName(Integer studentId, String studentName) throws Exception {
		Optional<Student> response = repository.findById(studentId);
		if (!response.isPresent()) {
			throw new Exception("student Data not found");
		}
		repository.update(studentId, studentName);
		return "Student details updated";
	}

	@Override
	public String deleteStudentData(Integer studentId) throws Exception {
		Optional<Student> response = repository.findById(studentId);
		if (!response.isPresent()) {
			throw new Exception("student Data not found");
		}
		repository.deleteById(studentId);
		return "Deleted Succesffully";
	}

}
