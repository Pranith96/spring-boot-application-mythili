package com.student.service;

import java.util.List;

import com.student.dto.StudentDto;
import com.student.entity.Student;
import com.student.exceptions.StudentNotFoundException;

public interface StudentService {

	Student create(Student student) throws Exception;

	List<Student> getStudentData() throws Exception;

	StudentDto getStudentDataByid(Integer studentId) throws StudentNotFoundException;

	Student update(Student student) throws Exception;

	String updateStudentName(Integer studentId, String studentName) throws Exception;

	String deleteStudentData(Integer studentId) throws Exception;

}
