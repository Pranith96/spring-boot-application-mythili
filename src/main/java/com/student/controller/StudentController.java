package com.student.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.student.dto.StudentDto;
import com.student.entity.Student;
import com.student.exceptions.StudentNotFoundException;
import com.student.service.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController {

	@Autowired
	StudentService studentService;

	@PostMapping("/save")
	public ResponseEntity<Student> createStudent(@RequestBody Student student) throws Exception {
		Student response = studentService.create(student);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@GetMapping("/get/all")
	public ResponseEntity<List<Student>> getAllStudent() throws Exception {
		List<Student> response = studentService.getStudentData();
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@GetMapping("/get/{studentId}")
	public ResponseEntity<StudentDto> getStudentDataByid(@PathVariable("studentId") Integer studentId) throws StudentNotFoundException {
		StudentDto response = studentService.getStudentDataByid(studentId);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@GetMapping("/get/id")
	public ResponseEntity<StudentDto> getStudentData(@RequestParam("studentId") Integer studentId) throws StudentNotFoundException {
		StudentDto response = studentService.getStudentDataByid(studentId);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@PutMapping("/update")
	public ResponseEntity<Student> updateStudent(@RequestBody Student student) throws Exception {
		Student response = studentService.update(student);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@PutMapping("/update/{studentId}/{studentName}")
	public ResponseEntity<String> updateStudentName(@PathVariable("studentId") Integer studentId,
			@PathVariable("studentName") String studentName) throws Exception {
		String response = studentService.updateStudentName(studentId, studentName);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@DeleteMapping("/delete/{studentId}")
	public ResponseEntity<String> deleteStudentData(@PathVariable("studentId") Integer studentId) throws Exception {
		String response = studentService.deleteStudentData(studentId);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

}
